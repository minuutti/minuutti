class Player
  constructor: (@node, @src) ->
    @init()

  init: ->
    @node.src = @src
    @node.load()

  timestamp: ->
    @progress()
      .map '.secondsToHms'
      .skipDuplicates()

  event: (type) ->
    Bacon.fromEventTarget(@node, type).map ".currentTarget"

  seek: (pos) =>
    @node.currentTime =  pos.hmsToSeconds()

  progress: ->
    @event("timeupdate").map(".currentTime").toProperty()

  play: =>
    @node.play()

  pause: =>
    @node.pause()

  addIntervalToSkip: (beginStr, endStr) ->
    begin = beginStr.hmsToSeconds()
    end = endStr.hmsToSeconds()
    player = this
    @progress().filter (time) ->
      begin < time < end
    .assign ->
      player.seek(end)

class AlertPlayer extends Player
  constructor: (@node, @src, @trigger) ->
    super(@node, @src)

  init: ->
    super()
    @trigger.onValue @play

class MinutesPlayer extends Player
  constructor: (@node, @srcFolder, @trigger) ->
    super(@node, @srcFolder + 'Saksi_Henri.mp4')

  show: (minute) =>
    @node.src = @srcFolder + Minutes.byNum minute
    $(@node).removeClass('video-' + (minute-1))
    $(@node).addClass('video-' + minute)
    console.log 'showing minute ' + minute
    $(@node).fadeTo(500, 1).delay(3000).fadeOut(500)

  init: ->
    super()
    @trigger.onValue @show
    @trigger.delay(500).onValue @play
