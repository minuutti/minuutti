class Timer
  constructor: (@initial = 0) ->
    @init()

  init: ->
    @runs = new Bacon.Bus()

  isRunning: -> @runs.toProperty(false);

  run: -> @runs.push true

  pause: -> @runs.push false

  plus = (a, b) -> a + b

  seconds: ->
    Bacon
      .interval 1000, 1
      .filter @isRunning()
      .scan @initial, plus

  minutes: (backwardOffset = 0) ->
    @seconds()
      .map (sec) ->
        parseInt (sec + backwardOffset) / 60, 10
      .skipDuplicates()

  timestamp: ->
    @seconds()
      .map '.secondsToHms'
      .skipDuplicates()
