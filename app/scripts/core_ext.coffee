String::hmsToSeconds = ->
  "00:#{this}".split(':').reduce (a,b) -> a * 60 + parseInt(b, 10)

Number::hmsToSeconds = "#{this}".hmsToSeconds

Number::secondsToHms = ->
  hours   = Math.floor(this / 3600)
  minutes = Math.floor((this - (hours * 3600)) / 60)
  seconds = Math.floor(this) - (hours * 3600) - (minutes * 60)

  leadingZero = (num) -> if num < 10 then "0#{num}" else "#{num}"

  "#{leadingZero hours}:#{leadingZero minutes}:#{leadingZero seconds}"

String::secondsToHms = ->
  parseInt(this, 10).secondsToHms()
