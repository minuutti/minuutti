class Counter
  constructor: (@node, @values) ->
    @init()

  init: ->
    @values.assign @update

  update: (value) =>
    $(@node).text(value).fadeTo(0, 0.8).delay(3000).fadeOut(500)
