$(document).ready ->
  minutes = LocationHash.get('m') || 0
  timer = new Timer minutes * 60

  starts = $('#controls .start').asEventStream('click').map(true)
  stops = $('#controls .stop').asEventStream('click').map(false)

  timer.runs.plug(stops.merge(starts))
  timer.timestamp().assign $('#controls .timer'), 'text'

  alerts = new AlertPlayer(
    document.getElementById('alerts'),
    '/sounds/buzzer.mp3',
    timer.minutes()
  )

  minutes = new MinutesPlayer(
    document.getElementById('minutes'),
    '/minutes/',
    timer.minutes(1)
  )

  counter = new Counter(
    document.getElementById('counter'),
    timer.minutes()
  )

  timer.minutes().assign (minutes) ->
    LocationHash.set('m', minutes)

  player = new Player(document.getElementById('player'), "/2011.mp4")

  $(player.node).one 'canplay', ->
    time = LocationHash.get('t')
    player.seek time if time

    player.timestamp().assign (time) -> LocationHash.set('t', time)

# # These intervals are for the 2011 game
  player.addIntervalToSkip '00:00:00', '00:02:20' # 1st
  player.addIntervalToSkip '00:11:35', '00:12:30'
  player.addIntervalToSkip '00:20:52', '00:21:47'
  player.addIntervalToSkip '00:35:38', '00:36:47' # 2nd
  player.addIntervalToSkip '00:58:32', '00:59:33'
  player.addIntervalToSkip '01:12:05', '01:12:06' # 3nd
  player.addIntervalToSkip '01:24:21', '01:25:17'

  player.node.volume = 1
  alerts.node.volume = 0.4
