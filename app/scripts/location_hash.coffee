class LocationHash

  objectFromHash = (hash) ->
    $.deparam(hash.substr(1))

  hashFromObject = (obj) ->
    "##{ decodeURIComponent $.param(obj) }"

  @object: ->
    objectFromHash(window.location.hash) || {}

  @get: (key) ->
    @object()[key]

  @set: (key, value) ->
    obj = @object()
    obj[key] = value
    window.location.hash = hashFromObject obj
