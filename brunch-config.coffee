exports.config =
  # See http://brunch.io/#documentation for docs.
  modules:
    definition: false
    wrapper: false

  files:
    javascripts:
      joinTo:
        'js/app.js': /^app/
        'js/vendor.js': /^(vendor|bower_components)/
      order:
        before: [
          'bower_components/jquery/dist/jquery.js',
          'app/scripts/core_ext.coffee'
        ],
        after: [
          'app/scripts/main.coffee'
        ]

    stylesheets:
      joinTo:
        'css/app.css': /^(app|vendor)/

    templates:
      joinTo: 'js/templates.js'

  server:
    noPushState: true
